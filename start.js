var express =require('express');
var Axios =require('axios');
var bodyParser = require('body-parser');
 
var app = express();
app.use(bodyParser.json());
app.use(express.static(__dirname + '/www'));

app.set('port',3000);
var server = app.listen( app.get('port'), function () {
console.log('Server listening on port' + server.address().port)
});

var FORGE_CLIENT_ID = process.env.FORGE_CLIENT_ID;
var FORGE_CLIENT_SECRET = process.env.FORGE_CLIENT_SECRET;

var access_token = '';
var scopes = 'data:read data:write data:create bucket:create bucket:read';
const querystring = require('querystring');

app.get('/api/forge/oauth', function(req, res){
  Axios({
    method: 'POST',
    url:'https://developer.api.autodesk.com/authentication/v1/authenticate',
    headers: {
      'content-type':'application/x-www-form-urlencoded',
    },
    data: querystring.stringify({
            client_id: FORGE_CLIENT_ID,
            client_secret: FORGE_CLIENT_SECRET,
            grant_type: 'client_credentials',
            scope: scopes
    })
  })
  .then(function(response){
    access_token =response.data.access_token;
    console.log(response);
    res.redirect('/api/forge/datamanagement/bucket/create')
  }).catch(function (error) {
    // Failed
    console.log(error);
    res.send('Failed to authenticate');
});
})

const bucketKey = FORGE_CLIENT_ID.toLowerCase() + '_tutorial_bucket';
const policyKey = 'transient';

app.get('/api/forge/datamanagement/bucket/create',function(req,res){
  Axios({
    method: 'POST',
    url: 'https://developer.api.autodesk.com/oss/v2/buckets',
    headers: {
        'content-type': 'application/json',
        Authorization: 'Bearer ' + access_token
    },
    data: JSON.stringify({
        'bucketKey': bucketKey,
        'policyKey': policyKey
    })
})
    .then(function (response) {
        // Success
        console.log(response);
        res.redirect('/api/forge/datamanagement/bucket/detail');
    })
    .catch(function (error) {
        if (error.response && error.response.status == 409) {
            console.log('Bucket already exists, skip creation.');
            res.redirect('/api/forge/datamanagement/bucket/detail');
        }
        // Failed
        console.log(error);
        res.send('Failed to create a new bucket');
    });
  })
app.get ('/api/forge/datamanagement/bucket/detail', function(req,res){
  Axios({
    method: 'GET',
    url: 'https://developer.api.autodesk.com/oss/v2/buckets/' + encodeURIComponent(bucketKey) + '/details',
    headers: {
        Authorization: 'Bearer ' + access_token
    }
})
    .then(function (response) {
        // Success
        console.log(response);
        res.redirect('/upload.html');
    })
    .catch(function (error) {
        // Failed
        console.log(error);
        res.send('Failed to verify the new bucket');
    });
  })
